<x-layouts.app>
    <x-slot:title>Dashboard</x-slot:title>
    <h1>{{ Auth::user()->name }}</h1>
    @livewire('mahasiswa.table')
</x-layouts.app>
