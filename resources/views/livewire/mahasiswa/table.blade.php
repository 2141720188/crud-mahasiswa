<div>
    @include('livewire.mahasiswa.modal')
    @if (session()->has('message'))
        <h5 class="alert alert-success">{{ session('message') }}</h5>
    @endif
    <div class="d-flex justify-content-end">
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#mahasiswaCreateModal" wire:click="resetInputFields">
            <i class="lni lni-plus"></i> Tambah
        </button>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col" class="text-uppercase">id</th>
                <th scope="col" class="text-uppercase">foto profil</th>
                <th scope="col" class="text-uppercase">nama lengkap</th>
                <th scope="col" class="text-uppercase">nim</th>
                <th scope="col" class="text-uppercase">aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($mahasiswas as $mahasiswa)
                <tr>
                    <td>{{ $mahasiswa->id }}</td>
                    <td>
                        <div class="square">
                            @if($mahasiswa->foto_profil == null)
                                <img src="https://nictodev.com/wp-content/uploads/2023/09/8ae9e92fa4e69967aa61bf2bda967b7b.jpg.webp" class="img-fluid rounded"
                                    alt="foto_profil">
                            @else
                            <img src="{{ asset('storage/' . $mahasiswa->foto_profil) }}" class="img-fluid rounded"
                                alt="foto_profil">
                            @endif
                        </div>
                    </td>
                    <td>{{ $mahasiswa->nama_lengkap }}</td>
                    <td>{{ $mahasiswa->nim }}</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-info" data-toggle="modal"
                            data-target="#mahasiswaReadModal" wire:click="detailMahasiswa({{ $mahasiswa->id }})">
                            <i class="lni lni-information"></i> Detail
                        </button>
                        <button type="button" class="btn btn-sm btn-warning" data-toggle="modal"
                            data-target="#mahasiswaUpdateModal" wire:click="editMahasiswa({{ $mahasiswa->id }})">
                            <i class="lni lni-pencil"></i> Edit
                        </button>
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal"
                            data-target="#mahasiswaDeleteModal" wire:click="deleteMahasiswa({{ $mahasiswa->id }})">
                            <i class="lni lni-trash-can"></i> Hapus
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $mahasiswas->links() }}
</div>
