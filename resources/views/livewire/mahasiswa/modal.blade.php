<div>
    <div wire:ignore.self class="modal fade" id="mahasiswaCreateModal" tabindex="-1" role="dialog"
        aria-labelledby="mahasiswaCreateModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mahasiswaCreateModalLabel">Tambah Mahasiswa </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="saveMahasiswa" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama_lengkap">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="nama_lengkap"
                                        placeholder="Masukkan Nama Lengkap" wire:model="nama_lengkap">
                                    @error('nama_lengkap')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nim">NIM</label>
                                    <input type="text" class="form-control" id="nim" placeholder="Masukkan NIM"
                                        wire:model="nim">
                                    @error('nim')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                            id="jenis_kelamin_l" value="L" wire:model="jenis_kelamin">
                                        <label class="form-check-label" for="jenis_kelamin_l">Laki-laki</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                            id="jenis_kelamin_p" value="P" wire:model="jenis_kelamin">
                                        <label class="form-check-label" for="jenis_kelamin_p">Perempuan</label>
                                    </div>
                                    @error('jenis_kelamin')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir">Tempat Lahir</label>
                                    <input type="text" class="form-control" id="tempat_lahir"
                                        placeholder="Masukkan Tempat Lahir" wire:model="tempat_lahir">
                                    @error('tempat_lahir')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="tanggal_lahir"
                                        wire:model="tanggal_lahir">
                                    @error('tanggal_lahir')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email"
                                        placeholder="Masukkan Email" wire:model="email">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nomor_telepon">Nomor Telepon</label>
                                    <input type="tel" class="form-control" id="nomor_telepon"
                                        placeholder="Masukkan Nomor Telepon" wire:model="nomor_telepon">
                                    @error('nomor_telepon')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="foto_profil">Foto Profile</label>
                                    <input type="file" accept="image/png, image/jpeg" class="form-control"
                                        id="foto_profil" wire:model="create_temp_foto_profil">
                                </div>
                                <div class="form-group">
                                    <div class="square">
                                        @if ($create_temp_foto_profil)
                                            <img src="{{ $create_temp_foto_profil->temporaryUrl() }}"
                                                class="img-fluid rounded" alt="foto_profil">
                                        @else
                                            <img src="https://nictodev.com/wp-content/uploads/2023/09/8ae9e92fa4e69967aa61bf2bda967b7b.jpg.webp"
                                                class="img-fluid rounded" alt="foto_profil">
                                        @endif
                                    </div>
                                    @error('create_temp_foto_profil')
                                        <span class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="lni lni-save"></i>
                            Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="mahasiswaReadModal" tabindex="-1" role="dialog"
        aria-labelledby="mahasiswaReadModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mahasiswaReadModalLabel">Detail Mahasiswa </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="h6">
                                Nama Lengkap
                            </p>
                            <p>
                                {{ $nama_lengkap }}
                            </p>
                            <p class="h6">
                                NIM
                            </p>
                            <p>
                                {{ $nim }}
                            </p>
                            <p class="h6">
                                Jenis Kelamin
                            </p>
                            <p>
                                {{ $jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan' }}
                            </p>
                            <p class="h6">
                                Tempat Lahir
                            </p>
                            <p>
                                {{ $tempat_lahir }}
                            </p>
                            <p class="h6">
                                Tanggal Lahir
                            </p>
                            <p>
                                {{ $tanggal_lahir }}
                            </p>
                            <p class="h6">
                                Email
                            </p>
                            <p>
                                {{ $email }}
                            </p>
                            <p class="h6">
                                Nomor Telepon
                            </p>
                            <p>
                                {{ $nomor_telepon }}
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="h6">
                                Foto Profil
                            </p>
                            <div class="square">
                                @if ($foto_profil == null)
                                    <img src="https://nictodev.com/wp-content/uploads/2023/09/8ae9e92fa4e69967aa61bf2bda967b7b.jpg.webp"
                                        class="img-fluid rounded" alt="foto_profil">
                                @else
                                    <img src="{{ asset('storage/' . $foto_profil) }}" class="img-fluid rounded"
                                        alt="foto_profil">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="mahasiswaUpdateModal" tabindex="-1" role="dialog"
        aria-labelledby="mahasiswaUpdateModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mahasiswaUpdateModalLabel">Edit Mahasiswa </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="updateMahasiswa" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama_lengkap">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="nama_lengkap"
                                        placeholder="Masukkan Nama Lengkap" wire:model="nama_lengkap">
                                    @error('nama_lengkap')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nim">NIM</label>
                                    <input type="text" class="form-control" id="nim"
                                        placeholder="Masukkan NIM" wire:model="nim">
                                    @error('nim')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                            id="jenis_kelamin_l" value="L" wire:model="jenis_kelamin">
                                        <label class="form-check-label" for="jenis_kelamin_l">Laki-laki</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin"
                                            id="jenis_kelamin_p" value="P" wire:model="jenis_kelamin">
                                        <label class="form-check-label" for="jenis_kelamin_p">Perempuan</label>
                                    </div>
                                    @error('jenis_kelamin')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tempat_lahir">Tempat Lahir</label>
                                    <input type="text" class="form-control" id="tempat_lahir"
                                        placeholder="Masukkan Tempat Lahir" wire:model="tempat_lahir">
                                    @error('tempat_lahir')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="tanggal_lahir"
                                        wire:model="tanggal_lahir">
                                    @error('tanggal_lahir')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email"
                                        placeholder="Masukkan Email" wire:model="email">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nomor_telepon">Nomor Telepon</label>
                                    <input type="tel" class="form-control" id="nomor_telepon"
                                        placeholder="Masukkan Nomor Telepon" wire:model="nomor_telepon">
                                    @error('nomor_telepon')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="foto_profil">Foto Profile</label>
                                    <input type="file" accept="image/png, image/jpeg" class="form-control"
                                        id="foto_profil" wire:model="foto_profil">
                                </div>
                                <div class="form-group">
                                    <div class="square">
                                        @if ($foto_profil == $update_temp_foto_profil)
                                            <img src="{{ asset('storage/' . $foto_profil) }}"
                                                class="img-fluid rounded" alt="foto_profil">
                                        @elseif ($foto_profil == null)
                                            <img src="https://nictodev.com/wp-content/uploads/2023/09/8ae9e92fa4e69967aa61bf2bda967b7b.jpg.webp"
                                                class="img-fluid rounded" alt="foto_profil">
                                        @elseif ($update_temp_foto_profil != null)
                                            <img src="{{ $foto_profil->temporaryUrl() }}" class="img-fluid rounded"
                                                alt="foto_profil">
                                        @endif
                                    </div>
                                    @error('foto_profil')
                                        <span class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-warning"><i class="lni lni-save"></i>
                            Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="mahasiswaDeleteModal" tabindex="-1" role="dialog"
        aria-labelledby="mahasiswaDeleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mahasiswaDeleteModalLabel">Hapus Mahasiswa </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form wire:submit.prevent="destroyMahasiswa" enctype="multipart/form-data">
                    <div class="modal-body">
                        <h4>Hapus {{ $nama_lengkap }}?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-danger"><i class="lni lni-trash-can"></i>
                            Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
