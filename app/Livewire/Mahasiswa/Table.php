<?php

namespace App\Livewire\Mahasiswa;

use Illuminate\Validation\Rule;

use App\Models\MahasiswaModel;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Table extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $id;
    public $nama_lengkap;
    public $nim;
    public $jenis_kelamin;
    public $tempat_lahir;
    public $tanggal_lahir;
    public $email;
    public $nomor_telepon;
    public $foto_profil;
    public $create_temp_foto_profil;
    public $update_temp_foto_profil;

    protected function rules()
    {
        $rules = [
            'nama_lengkap' => 'required|string|max:255',
            'nim' => ['required', 'numeric', Rule::unique('mahasiswa')->ignore($this->id)],
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required|string|max:255',
            'tanggal_lahir' => 'required|date',
            'email' => ['required', 'email', Rule::unique('mahasiswa')->ignore($this->id)],
            'nomor_telepon' => ['required', 'numeric', Rule::unique('mahasiswa')->ignore($this->id)],
        ];

        if ($this->foto_profil == $this->update_temp_foto_profil) {
            $rules['foto_profil'] = 'nullable';
        } else {
            $rules['foto_profil'] = 'nullable|image|max:1024';
        }

        return $rules;
    }


    public function updated($fields)
    {
        $this->validateOnly($fields);
    }

    public function resetInputFields()
    {
        $this->nama_lengkap = '';
        $this->nim = '';
        $this->jenis_kelamin = '';
        $this->tempat_lahir = '';
        $this->tanggal_lahir = '';
        $this->email = '';
        $this->nomor_telepon = '';
        $this->foto_profil = '';
        $this->update_temp_foto_profil = '';
        $this->create_temp_foto_profil = '';
    }

    public function saveMahasiswa()
    {
        $validated = $this->validate();

        if ($this->create_temp_foto_profil) {
            $validated['foto_profil'] = $this->create_temp_foto_profil->store('foto_profil', 'public');
        }

        MahasiswaModel::create($validated);

        session()->flash('message', 'Mahasiswa Created Successfully');

        $this->reset('nama_lengkap', 'nim', 'jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'email', 'nomor_telepon', 'foto_profil');
        $this->dispatch('close-modal');
    }

    public function detailMahasiswa($id)
    {
        $this->resetInputFields();
        $mahasiswa = MahasiswaModel::find($id);
        $this->id = $mahasiswa->id;
        $this->nama_lengkap = $mahasiswa->nama_lengkap;
        $this->nim = $mahasiswa->nim;
        $this->jenis_kelamin = $mahasiswa->jenis_kelamin;
        $this->tempat_lahir = $mahasiswa->tempat_lahir;
        $this->tanggal_lahir = $mahasiswa->tanggal_lahir;
        $this->email = $mahasiswa->email;
        $this->nomor_telepon = $mahasiswa->nomor_telepon;
        $this->foto_profil = $mahasiswa->foto_profil;
    }

    public function editMahasiswa($id)
    {
        $this->resetInputFields();
        $mahasiswa = MahasiswaModel::find($id);
        $this->id = $mahasiswa->id;
        $this->nama_lengkap = $mahasiswa->nama_lengkap;
        $this->nim = $mahasiswa->nim;
        $this->jenis_kelamin = $mahasiswa->jenis_kelamin;
        $this->tempat_lahir = $mahasiswa->tempat_lahir;
        $this->tanggal_lahir = $mahasiswa->tanggal_lahir;
        $this->email = $mahasiswa->email;
        $this->nomor_telepon = $mahasiswa->nomor_telepon;
        $this->foto_profil = $mahasiswa->foto_profil;
        $this->foto_profil = $mahasiswa->foto_profil;
        $this->update_temp_foto_profil = $mahasiswa->foto_profil;
    }

    public function updateMahasiswa()
    {
        $validated = $this->validate();

        if ($this->foto_profil != $this->update_temp_foto_profil) {
            Storage::disk('public')->delete($this->update_temp_foto_profil);

            $validated['foto_profil'] = $this->foto_profil->store('foto_profil', 'public');
        }

        MahasiswaModel::where('id', $this->id)->update($validated);
        session()->flash('message', 'Mahasiswa Updated Successfully');
        $this->dispatch('close-modal');
    }

    public function deleteMahasiswa($id)
    {
        $mahasiswa = MahasiswaModel::find($id);
        $this->id = $mahasiswa->id;
        $this->nama_lengkap = $mahasiswa->nama_lengkap;
        $this->foto_profil = $mahasiswa->foto_profil;
    }

    public function destroyMahasiswa()
    {
        MahasiswaModel::destroy($this->id);
        Storage::disk('public')->delete($this->foto_profil);
        session()->flash('message', 'Mahasiswa Deleted Successfully');
        $this->dispatch('close-modal');
    }

    public function render()
    {
        $mahasiswas = MahasiswaModel::orderBy('id', 'asc')->paginate(3);
        return view('livewire.mahasiswa.table')
            ->with('mahasiswas', $mahasiswas);
    }
}
