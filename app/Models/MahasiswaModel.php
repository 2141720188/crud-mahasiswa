<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswaModel extends Model
{
    use HasFactory;
    protected $table = "mahasiswa";
    protected $fillable = [
        'nama_lengkap',
        'nim',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'email',
        'nomor_telepon',
        'foto_profil'
    ];
}
